# gtk4-memory-management

This repository explains how to properly do memory management in gtk4.

## Resources:
Got this code from GUI development with Rust and GTK 4 Book:
* [Memory Management in gtk4](https://gtk-rs.org/gtk4-rs/stable/latest/book/g_object_memory_management.html#memory-management)
